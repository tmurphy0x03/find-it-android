package org.findit.android.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Branch extends BranchData implements Parcelable{
	
    public static final Parcelable.Creator<Branch> CREATOR
            = new Parcelable.Creator<Branch>() {
        public Branch createFromParcel(Parcel in) {
            return new Branch(in);
        }

        public Branch[] newArray(int size) {
            return new Branch[size];
        }
    };
    
    private Branch(Parcel in) {
        setName(in.readString());
        setAddress1(in.readString());
        setAddress2(in.readString());
        setAddress3(in.readString());
        setCountry(in.readString());
        setLatitude(in.readString());
        setLocation(in.readString());
        setLongitude(in.readString());
        setPhoneNumber(in.readString());
        setPostcode(in.readString());
        setProperty(in.readString());
        setRegion(in.readString());
        setRetailerName(in.readString());
        setRetailerUsername(in.readString());
        setHits(in.readInt());
        setId(in.readInt());
        setEmail(in.readString());
    }

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(getName());
		dest.writeString(getAddress1());
		dest.writeString(getAddress2());
		dest.writeString(getAddress3());
		dest.writeString(getCountry());
		dest.writeString(getLatitude());
		dest.writeString(getLocation());
		dest.writeString(getLongitude());
		dest.writeString(getPhoneNumber());
		dest.writeString(getPostcode());
		dest.writeString(getProperty());
		dest.writeString(getRegion());
		dest.writeString(getRetailerName());
		dest.writeString(getRetailerUsername());
		dest.writeInt(getHits());
		dest.writeInt(getId());
		dest.writeString(getEmail());
	}
}
