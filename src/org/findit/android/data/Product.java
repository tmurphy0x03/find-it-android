package org.findit.android.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Product extends ProductData implements Parcelable{
	
    public static final Parcelable.Creator<Product> CREATOR
            = new Parcelable.Creator<Product>() {
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
    
    private Product(Parcel in) {
        setName(in.readString());
        setCategory(in.readString());
        setDescription(in.readString());
        setHits(in.readInt());
        setId(in.readInt());
        setPrice(in.readDouble());
        setRetailerName(in.readString());
        setRetailerUsername(in.readString());
        setLongitude(in.readString());
        setLatitude(in.readString());
        setBranchName(in.readString());
        setBranchId(in.readInt());
        setAddress1(in.readString());
        setAddress2(in.readString());
        setAddress3(in.readString());
        setCountry(in.readString());
        setLocation(in.readString());
        setPostcode(in.readString());
        setRegion(in.readString());
        setProperty(in.readString());
    }

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(getName());
		dest.writeString(getCategory());
		dest.writeString(getDescription());
		dest.writeInt(getHits());
		dest.writeInt(getId());
		dest.writeDouble(getPrice());
		dest.writeString(getRetailerName());
		dest.writeString(getRetailerUsername());
		dest.writeString(getLongitude());
		dest.writeString(getLatitude());
		dest.writeString(getBranchName());
		dest.writeInt(getBranchId());
		dest.writeString(getAddress1());
		dest.writeString(getAddress2());
		dest.writeString(getAddress3());
		dest.writeString(getCountry());
		dest.writeString(getLocation());
		dest.writeString(getPostcode());
		dest.writeString(getRegion());
		dest.writeString(getProperty());
	}
}
