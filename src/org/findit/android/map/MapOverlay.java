package org.findit.android.map;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;

public class MapOverlay extends ItemizedOverlay {
	private ArrayList<OverlayItem> overlays = new ArrayList<OverlayItem>();
	private Context context;

	public MapOverlay(Drawable marker, Context context) {
		super(boundCenterBottom(marker));
		this.context = context;
	}

	@Override
	protected OverlayItem createItem(int i) {
		return overlays.get(i);
	}

	@Override
	public int size() {
		return overlays.size();
	}

	@Override
	protected boolean onTap(int index){
		OverlayItem item = overlays.get(index);
		// build an alert dialog to show to the user
		AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		// add the overlay item's title and snippet
		dialog.setTitle(item.getTitle());
		dialog.setMessage(item.getSnippet());
		dialog.show();
		// I have consumed this click, don't pass on to other overlays
		return true;
	}

	public void addOverlay(OverlayItem overlay) {
		overlays.add(overlay);
		populate();
	}
}
