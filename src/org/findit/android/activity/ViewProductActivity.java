package org.findit.android.activity;

import org.findit.android.R;
import org.findit.android.data.Product;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ViewProductActivity extends Activity {
	private TextView name;
	private TextView category;
	private TextView priceNumber;
	private TextView retailerName;
	private TextView description;
	private TextView hits;
	private TextView retailer;
	private Button viewOnMapButton;
	private Button getDirectionsButton;
	private Product product;

	/** Called when the activity is first created */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_product);
		
		Bundle bundle = getIntent().getExtras();
		product = (Product) bundle.get("product");
		Log.d("PRODUCT", product.getLongitude());
		name = (TextView) findViewById(R.id.product_name_value);
		if(product.getName().equals("")){
			findViewById(R.id.product_name).setVisibility(View.GONE);
		}
		else {
			name.setText(product.getName()+" ");
		}
		
		priceNumber = (TextView) findViewById(R.id.product_price_value);
		priceNumber.setText(product.getPrice()+" ");
		
		category = (TextView) findViewById(R.id.product_category_value);
		if(product.getCategory().equals("")){
			findViewById(R.id.product_category).setVisibility(View.GONE);
		}
		else {
			category.setText(product.getCategory()+" ");
		}
		
		retailerName = (TextView) findViewById(R.id.product_retailer_value);
		if(product.getRetailerName().equals("")){
			findViewById(R.id.product_retailer).setVisibility(View.GONE);
		}
		else {
			retailerName.setText(product.getRetailerName()+" ");
		}

		description = (TextView) findViewById(R.id.product_description_value);
		if(product.getDescription().equals("")){
			findViewById(R.id.product_description).setVisibility(View.GONE);
		}
		else {
			description.setText(product.getDescription()+" ");
		}
		
		hits = (TextView) findViewById(R.id.product_hits_value);
		hits.setText(product.getHits()+" ");
			
		retailer = (TextView) findViewById(R.id.product_retailer_value);
		if(product.getRetailerName().equals("")){
			findViewById(R.id.product_retailer).setVisibility(View.GONE);
		}
		else {
			retailer.setText(product.getRetailerName()+" ");
		}
		
		viewOnMapButton = (Button) findViewById(R.id.product_map);
		viewOnMapButton.setOnClickListener(viewOnMapListener);
		
		getDirectionsButton = (Button) findViewById(R.id.product_directions);
		getDirectionsButton.setOnClickListener(getDirectionsListener);
	}
	
	/**
	 *  View on Map button listener 
	 */
	private OnClickListener viewOnMapListener = new OnClickListener(){            
		public void onClick(View v) {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			Bundle bundle = new Bundle();
			bundle.putParcelable("product", product);
			intent.putExtras(bundle);
			intent.setClassName(getApplicationContext(), ViewProductMapActivity.class.getName());
			startActivity(intent);
		}          
	}; 
	
	/**
	 *  View on Map button listener 
	 */
	private OnClickListener getDirectionsListener = new OnClickListener(){            
		public void onClick(View v) {
			LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			String bestProvider = locationManager.getBestProvider(criteria, false);
			Location location = locationManager.getLastKnownLocation(bestProvider);
			String url = "http://maps.google.com/maps?f=d&saddr="+location.getLatitude()+"%20"+location.getLongitude()+"&daddr="+product.getLatitude()+"%20"+product.getLongitude()+"&hl=en";
			Uri uri = Uri.parse(url);
			Log.d("URL", url);
			Intent intent = new Intent(Intent.ACTION_VIEW, uri);
			startActivity(intent);
		}          
	}; 
}
