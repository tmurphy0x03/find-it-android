package org.findit.android.activity;

import java.util.List;

import org.findit.android.R;
import org.findit.android.data.Branch;
import org.findit.android.data.Product;
import org.findit.android.map.MapOverlay;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class ViewProductMapActivity extends MapActivity implements LocationListener {
	private MapView mapView;
	private Product product;
	private GeoPoint currentLocation;
	private MapController mapController;
	private List<Overlay> mapOverlays;
	private Drawable finditPin;
	private Drawable userPin;
	private MapOverlay userOverlay;
	
	/* Called on creation */
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_product_map);
		
		mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);
		
		Bundle bundle = getIntent().getExtras();
		product = (Product) bundle.get("product");
		
		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
		Criteria criteria = new Criteria();
		String bestProvider = locationManager.getBestProvider(criteria, false);
		
		Location location = locationManager.getLastKnownLocation(bestProvider);
		
		locationManager.requestLocationUpdates(bestProvider, 300000, 5000, this);
		
		mapOverlays = mapView.getOverlays();
		finditPin = this.getResources().getDrawable(R.drawable.pin);
		userPin = this.getResources().getDrawable(R.drawable.user_pin);
		MapOverlay productOverlay = new MapOverlay(finditPin, this);
		userOverlay = new MapOverlay(userPin, this);
		
		GeoPoint productPoint = new GeoPoint((int) (Double.parseDouble(product.getLatitude())*1000000), (int) (Double.parseDouble(product.getLongitude())*1000000));
		OverlayItem overlayitem = new OverlayItem(productPoint, product.getRetailerName(), assembleProductDetails(product));
		productOverlay.addOverlay(overlayitem);
		
		GeoPoint currentLocation = new GeoPoint((int) (location.getLatitude()*1000000), (int) (location.getLongitude()*1000000));
		overlayitem = new OverlayItem(currentLocation, "Me", "My Last Known Location");
		userOverlay.addOverlay(overlayitem);
		
		mapOverlays.add(productOverlay);
		mapOverlays.add(userOverlay);
		
		mapController = mapView.getController();
		mapController.setCenter(productPoint);
		mapController.setZoom(16);
	}
	
	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void onLocationChanged(Location location) {
		mapView.getOverlays().remove(userOverlay);
		currentLocation = new GeoPoint((int) (location.getLatitude()*1000000), (int) (location.getLongitude()*1000000));
		OverlayItem overlayitem = new OverlayItem(currentLocation, "Me", "My Location");
		userOverlay = new MapOverlay(userPin, this);
		userOverlay.addOverlay(overlayitem);
		mapView.getOverlays().add(userOverlay);
		mapView.refreshDrawableState();
		mapView.invalidate();
	}

	@Override
	public void onProviderDisabled(String provider) {
		Toast.makeText(this, "GPS Disabled", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onProviderEnabled(String provider) {
		Toast.makeText(this, "GPS Enabled", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	} 
	
	private String assembleProductDetails(Product product){
		String productDetails = product.getName()+":";
		if(!product.getBranchName().equals("")){
			productDetails += "\n"+product.getBranchName()+",";
		}
		if(!product.getProperty().equals("")){
			productDetails += "\n"+product.getProperty()+",";
		}
		if(!product.getAddress1().equals("")){
			productDetails += "\n"+product.getAddress1()+",";
		}
		if(!product.getAddress2().equals("")){
			productDetails += "\n"+product.getAddress2()+",";
		}
		if(!product.getAddress3().equals("")){
			productDetails += "\n"+product.getAddress3()+",";
		}
		if(!product.getLocation().equals("")){
			productDetails += "\n"+product.getLocation()+",";
		}
		if(!product.getRegion().equals("")){
			productDetails += "\n"+product.getRegion()+",";
		}
		if(!product.getPostcode().equals("")){
			productDetails += "\n"+product.getPostcode()+",";
		}
		if(!product.getCountry().equals("")){
			productDetails += "\n"+product.getCountry()+",";
		}
		return productDetails;
	}
}
