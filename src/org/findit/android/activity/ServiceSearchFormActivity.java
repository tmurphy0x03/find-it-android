package org.findit.android.activity;

import org.findit.android.R;
import org.findit.android.deployment.DeploymentVariables;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

public class ServiceSearchFormActivity extends Activity{
	private static final String TAG = "ServiceSearchFormActivity";
	private EditText nameEditText;
	private EditText descriptionEditText;
	private Spinner categorySpinner;
	private ArrayAdapter<CharSequence> optionListAdapter;
	private ImageView searchButton;
	private final String baseUrl = DeploymentVariables.baseUrl;
	/** Called when the activity is first created */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.service_search_form);
		
		nameEditText = (EditText) findViewById(R.id.service_name_value);
		descriptionEditText = (EditText) findViewById(R.id.service_description_value);
		searchButton = (ImageView) findViewById(R.id.service_search);
		searchButton.setOnClickListener(searchListener);
		
		/* Drop down list of values to top up by */
		categorySpinner = (Spinner) findViewById(R.id.service_category_value);
        optionListAdapter = ArrayAdapter.createFromResource(this, R.array.service_category_options, android.R.layout.simple_spinner_item);
        optionListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(optionListAdapter);
	}

	/**
	 *  Balance button listener 
	 */
	private OnClickListener searchListener = new OnClickListener(){            
		public void onClick(View v) {
			LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			String bestProvider = locationManager.getBestProvider(criteria, false);
			Location location = locationManager.getLastKnownLocation(bestProvider);
			
			String url = baseUrl+"serviceSearch.html?name="+nameEditText.getText().toString()
			+"&category="+categorySpinner.getSelectedItem().toString()
			+"&description="+descriptionEditText.getText().toString()
			+"&longitude="+location.getLongitude()
			+"&latitude="+location.getLatitude();
			Log.d(TAG, url);
			
			Intent intent = new Intent(Intent.ACTION_VIEW);
			Bundle bundle = new Bundle();
			bundle.putString("url", url);
			intent.putExtras(bundle);
			intent.setClassName(getApplicationContext(), ServiceSearchResultListActivity.class.getName());
			startActivity(intent);
		}          
	}; 
}
