package org.findit.android.activity;

import org.findit.android.R;
import org.findit.android.deployment.DeploymentVariables;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;

public class BranchSearchFormActivity extends Activity {
	private static final String TAG = "BranchSearchFormActivity";
	private EditText nameEditText;
	private EditText locationEditText;
	private EditText retailerNameEditText;
	private ImageView searchButton;
	private final String baseUrl = DeploymentVariables.baseUrl;
	
	/** Called when the activity is first created */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.branch_search_form);
		
		nameEditText = (EditText) findViewById(R.id.branch_name_value);
		locationEditText = (EditText) findViewById(R.id.branch_location_value);
		retailerNameEditText = (EditText) findViewById(R.id.branch_retailerName_value);
		searchButton = (ImageView) findViewById(R.id.branch_search);
		searchButton.setOnClickListener(searchListener);
		
	}

	/**
	 *  Balance button listener 
	 */
	private OnClickListener searchListener = new OnClickListener(){            
		public void onClick(View v) {
			LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			String bestProvider = locationManager.getBestProvider(criteria, false);
			Location location = locationManager.getLastKnownLocation(bestProvider);
			
			String url = baseUrl+"branchSearch.html?name="+nameEditText.getText().toString()
			+"&location="+locationEditText.getText().toString()
			+"&retailerName="+retailerNameEditText.getText().toString()
			+"&longitude="+location.getLongitude()
			+"&latitude="+location.getLatitude();
			Log.d(TAG, url);
			
			Intent intent = new Intent(Intent.ACTION_VIEW);
			Bundle bundle = new Bundle();
			bundle.putString("url", url);
			intent.putExtras(bundle);
			intent.setClassName(getApplicationContext(), BranchSearchResultListActivity.class.getName());
			startActivity(intent);
		}          
	};
}
