package org.findit.android.activity;

import java.util.List;

import org.findit.android.R;
import org.findit.android.data.Branch;
import org.findit.android.data.Product;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ViewBranchActivity extends Activity {
	private static final String TAG = "ViewBranchActivity";
	private TextView name;
	private TextView property;
	private TextView phoneNumber;
	private TextView email;
	private TextView retailerName;
	private TextView address1;
	private TextView address2;
	private TextView address3;
	private TextView location;
	private TextView region;
	private TextView postcode;
	private TextView country;
	private Button viewOnMapButton;
	private Button getDirectionsButton;
	private Branch branch;

	/** Called when the activity is first created */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_branch);
		
		
		Bundle bundle = getIntent().getExtras();
		branch = (Branch) bundle.get("branch");
		Log.d("dDDDDDDDDDDDDDDDDDDD"+getLocalClassName(), branch.toString());

		name = (TextView) findViewById(R.id.branch_name_value);
		if(branch.getName().equals("")){
			findViewById(R.id.branch_name).setVisibility(View.GONE);
		}
		else {
			name.setText(branch.getName()+" ");
		}
		
		phoneNumber = (TextView) findViewById(R.id.branch_phone_value);
		if(branch.getPhoneNumber().equals("")){
			findViewById(R.id.branch_phone).setVisibility(View.GONE);
		}
		else {
			/* Underline the Phone Number */
			SpannableString content = new SpannableString(branch.getPhoneNumber());
		    content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
			phoneNumber.setText(content);
			phoneNumber.setOnClickListener(callBranchListener);
		}
		
		email = (TextView) findViewById(R.id.branch_email_value);
		if(branch.getEmail().equals("")){
			findViewById(R.id.branch_email).setVisibility(View.GONE);
		}
		else {
			/* Underline the Phone Number */
			SpannableString content = new SpannableString(branch.getEmail());
		    content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
			email.setText(content);
			email.setOnClickListener(emailBranchListener);
		}
		
		property = (TextView) findViewById(R.id.branch_property_value);
		if(branch.getProperty().equals("")){
			findViewById(R.id.branch_property).setVisibility(View.GONE);
		}
		else {
			property.setText(branch.getProperty()+" ");
		}
		
		retailerName = (TextView) findViewById(R.id.branch_retailer_value);
		if(branch.getRetailerName().equals("")){
			findViewById(R.id.branch_retailer).setVisibility(View.GONE);
		}
		else {
			retailerName.setText(branch.getRetailerName()+" ");
		}
		
		address1 = (TextView) findViewById(R.id.branch_address1_value);
		if(branch.getAddress1().equals("")){
			findViewById(R.id.branch_address1).setVisibility(View.GONE);
		}
		else {
			address1.setText(branch.getAddress1()+" ");
		}
		
		address2 = (TextView) findViewById(R.id.branch_address2_value);
		if(branch.getAddress2().equals("")){
			findViewById(R.id.branch_address2).setVisibility(View.GONE);
		}
		else {
			address2.setText(branch.getAddress2()+" ");
		}
			
		address3 = (TextView) findViewById(R.id.branch_address3_value);
		if(branch.getAddress3().equals("")){
			findViewById(R.id.branch_address3).setVisibility(View.GONE);
		}
		else {
			address3.setText(branch.getAddress3()+" ");
		}
		
		location = (TextView) findViewById(R.id.branch_location_value);
		if(branch.getLocation().equals("")){
			findViewById(R.id.branch_location).setVisibility(View.GONE);
		}
		else {
			location.setText(branch.getLocation()+" ");
		}
		
		region = (TextView) findViewById(R.id.branch_region_value);
		if(branch.getRegion().equals("")){
			findViewById(R.id.branch_region).setVisibility(View.GONE);
		}
		else {
			region.setText(branch.getRegion()+" ");
		}
		
		postcode = (TextView) findViewById(R.id.branch_postcode_value);
		if(branch.getPostcode().equals("")){
			postcode.setVisibility(View.GONE);
			findViewById(R.id.branch_postcode).setVisibility(View.GONE);
		}
		else {
			postcode.setText(branch.getPostcode()+" ");
		}
		
		country = (TextView) findViewById(R.id.branch_country_value);
		if(branch.getCountry().equals("")){
			country.setVisibility(View.GONE);
			findViewById(R.id.branch_country).setVisibility(View.GONE);
		}
		else {
			country.setText(branch.getCountry()+" ");
		}
		
		viewOnMapButton = (Button) findViewById(R.id.branch_map);
		viewOnMapButton.setOnClickListener(viewOnMapListener);
		
		getDirectionsButton = (Button) findViewById(R.id.branch_directions);
		getDirectionsButton.setOnClickListener(getDirectionsListener);
	}
	
	/**
	 *  View on Map button listener 
	 */
	private OnClickListener viewOnMapListener = new OnClickListener(){            
		public void onClick(View v) {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			Bundle bundle = new Bundle();
			bundle.putParcelable("branch", branch);
			intent.putExtras(bundle);
			intent.setClassName(getApplicationContext(), ViewBranchMapActivity.class.getName());
			startActivity(intent);
		}          
	}; 
	
	/**
	 *  View on Map button listener 
	 */
	private OnClickListener getDirectionsListener = new OnClickListener(){            
		public void onClick(View v) {
			LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			String bestProvider = locationManager.getBestProvider(criteria, false);
			Location location = locationManager.getLastKnownLocation(bestProvider);
			String url = "http://maps.google.com/maps?f=d&saddr="+location.getLatitude()+"%20"+location.getLongitude()+"&daddr="+branch.getLatitude()+"%20"+branch.getLongitude()+"&hl=en";
			Uri uri = Uri.parse(url);
			Log.d("URL", url);
			Intent intent = new Intent(Intent.ACTION_VIEW, uri);
			startActivity(intent);
		}          
	}; 
	
	/**
	 *  Call Branch listener 
	 */
	private OnClickListener callBranchListener = new OnClickListener(){            
		public void onClick(View v) {
			/* Make phone call to BOI Voice Banking Service */
			String number = "tel:"+branch.getPhoneNumber();
			Intent intent = new Intent(Intent.ACTION_CALL);
			intent.setData(Uri.parse(number));
			startActivity(intent);
		}          
	}; 
	
	/**
	 *  Email Branch listener 
	 */
	private OnClickListener emailBranchListener = new OnClickListener(){            
		public void onClick(View v) {
			final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            emailIntent.setType("plain/text");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ email.getText().toString()});
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Android User Query");
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
		}          
	}; 
}
