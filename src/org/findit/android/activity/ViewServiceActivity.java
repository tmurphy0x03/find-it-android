package org.findit.android.activity;

import org.findit.android.R;
import org.findit.android.data.Service;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ViewServiceActivity extends Activity {
	private TextView name;
	private TextView category;
	private TextView priceNumber;
	private TextView retailerName;
	private TextView description;
	private TextView hits;
	private TextView retailer;
	private Button viewOnMapButton;
	private Button getDirectionsButton;
	private Service service;

	/** Called when the activity is first created */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_service);
		
		Bundle bundle = getIntent().getExtras();
		service = (Service) bundle.get("service");

		name = (TextView) findViewById(R.id.service_name_value);
		if(service.getName().equals("")){
			findViewById(R.id.service_name).setVisibility(View.GONE);
		}
		else {
			name.setText(service.getName()+" ");
		}
		
		priceNumber = (TextView) findViewById(R.id.service_price_value);
		priceNumber.setText(service.getPrice()+" ");
		
		category = (TextView) findViewById(R.id.service_category_value);
		if(service.getCategory().equals("")){
			findViewById(R.id.service_category).setVisibility(View.GONE);
		}
		else {
			category.setText(service.getCategory()+" ");
		}
		
		retailerName = (TextView) findViewById(R.id.service_retailer_value);
		if(service.getRetailerName().equals("")){
			findViewById(R.id.service_retailer).setVisibility(View.GONE);
		}
		else {
			retailerName.setText(service.getRetailerName()+" ");
		}

		description = (TextView) findViewById(R.id.service_description_value);
		if(service.getDescription().equals("")){
			findViewById(R.id.service_description).setVisibility(View.GONE);
		}
		else {
			description.setText(service.getDescription()+" ");
		}
		
		hits = (TextView) findViewById(R.id.service_hits_value);
		hits.setText(service.getHits()+" ");
			
		retailer = (TextView) findViewById(R.id.service_retailer_value);
		if(service.getRetailerName().equals("")){
			findViewById(R.id.service_retailer).setVisibility(View.GONE);
		}
		else {
			retailer.setText(service.getRetailerName()+" ");
		}
		
		viewOnMapButton = (Button) findViewById(R.id.service_map);
		viewOnMapButton.setOnClickListener(viewOnMapListener);
		
		getDirectionsButton = (Button) findViewById(R.id.service_directions);
		getDirectionsButton.setOnClickListener(getDirectionsListener);
	}
	
	/**
	 *  View on Map button listener 
	 */
	private OnClickListener viewOnMapListener = new OnClickListener(){            
		public void onClick(View v) {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			Bundle bundle = new Bundle();
			bundle.putParcelable("service", service);
			intent.putExtras(bundle);
			intent.setClassName(getApplicationContext(), ViewServiceMapActivity.class.getName());
			startActivity(intent);
		}          
	}; 
	
	/**
	 *  View on Map button listener 
	 */
	private OnClickListener getDirectionsListener = new OnClickListener(){            
		public void onClick(View v) {
			LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			String bestProvider = locationManager.getBestProvider(criteria, false);
			Location location = locationManager.getLastKnownLocation(bestProvider);
			String url = "http://maps.google.com/maps?f=d&saddr="+location.getLatitude()+"%20"+location.getLongitude()+"&daddr="+service.getLatitude()+"%20"+service.getLongitude()+"&hl=en";
			Uri uri = Uri.parse(url);
			Log.d("URL", url);
			Intent intent = new Intent(Intent.ACTION_VIEW, uri);
			startActivity(intent);
		}          
	}; 
}
