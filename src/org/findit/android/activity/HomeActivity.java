package org.findit.android.activity;

import org.findit.android.R;
import org.findit.android.deployment.DeploymentVariables;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class HomeActivity extends Activity {
	private static final String TAG = "HomeActivity";
	private Button productSearchButton;
	private Button serviceSearchButton;
	private Button branchSearchButton;
	private Button whatsAroundMeButton;
	private String baseUrl = DeploymentVariables.baseUrl;
	
	/** Called when the activity is first created */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		
		/* Product Search button */
		productSearchButton = (Button) findViewById(R.id.search_product);
		productSearchButton.setOnClickListener(productSearchListener);
        
		/* Service Search button */
		serviceSearchButton = (Button) findViewById(R.id.search_service);
		serviceSearchButton.setOnClickListener(serviceSearchListener);
        
		/* Branch Search button */
		branchSearchButton = (Button) findViewById(R.id.search_branch);
		branchSearchButton.setOnClickListener(branchSearchListener);
        
        /* What's Around Me button */
		whatsAroundMeButton = (Button) findViewById(R.id.whats_around_me);
		whatsAroundMeButton.setOnClickListener(whatsAroundMeListener);
	}
	
	/**
	 *  Balance button listener 
	 */
	private OnClickListener productSearchListener = new OnClickListener(){            
		public void onClick(View v) {
			/* Start ProductSearchActivity */
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setClassName(getApplicationContext(), ProductSearchFormActivity.class.getName());
			startActivity(intent);
			Log.i(TAG, "Starting ProductSearchActivity");
		}          
	}; 
	
	/**
	 *  Mini Statement button listener 
	 */
	private OnClickListener serviceSearchListener = new OnClickListener(){            
		public void onClick(View v) {
//			/* Start ServiceSearchActivity */
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setClassName(getApplicationContext(), ServiceSearchFormActivity.class.getName());
			startActivity(intent);
			Log.i(TAG, "Starting ServiceSearchActivity");
		}          
	};  
	
	/**
	 *  Top Up button listener 
	 */
	private OnClickListener branchSearchListener = new OnClickListener(){            
		public void onClick(View v) {
			/* Start ProductSearchActivity */
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setClassName(getApplicationContext(), BranchSearchFormActivity.class.getName());
			startActivity(intent);
			Log.i(TAG, "Starting BranchSearchActivity");
		}          
	};  
	
	/**
	 *  Branch Locator button listener 
	 */
	private OnClickListener whatsAroundMeListener = new OnClickListener(){            
		public void onClick(View v) {
			LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			String bestProvider = locationManager.getBestProvider(criteria, false);
			Location location = locationManager.getLastKnownLocation(bestProvider);
			String url = baseUrl+"whatsAroundMe.html?name="+"&longitude="+location.getLongitude()+"&latitude="+location.getLatitude();
			Log.d(TAG, url);
			
			Intent intent = new Intent(Intent.ACTION_VIEW);
			Bundle bundle = new Bundle();
			bundle.putString("url", url);
			intent.putExtras(bundle);
			intent.setClassName(getApplicationContext(), WhatsAroundMeMapActivity.class.getName());
			startActivity(intent);
		}          
	};  
}