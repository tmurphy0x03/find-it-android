package org.findit.android.activity;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.findit.android.R;
import org.findit.android.data.Branch;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class BranchSearchResultListActivity extends ListActivity{
	private static final String TAG = "BranchSearchResultListActivity";
	private List<String> branchInfo;
	private LinkedList<Branch> branches;
	
	/* Called on creation */
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.branch_search_results_list);
		
		/* Register a context menu to be shown for a list view */ 
		registerForContextMenu(getListView());
		
		DefaultHttpClient httpClient = new DefaultHttpClient();
		URI uri;
		InputStream data = null;
		Bundle extras = this.getIntent().getExtras();
		String url = extras.getString("url");
		try {
			uri = new URI(url);
			HttpGet method = new HttpGet(uri);
			method.setHeader("Accept", "application/json");
			HttpResponse response = httpClient.execute(method);
			data = response.getEntity().getContent();
		
			Gson gson = new Gson();
			Reader reader = new InputStreamReader(data);
			Type collectionType = new TypeToken<Collection<Branch>>(){}.getType();
			Collection<Branch> branchCollection = gson.fromJson(reader, collectionType);
			branches = new LinkedList<Branch>(branchCollection);
			Log.d(TAG, branches.toString());
			branchInfo = new LinkedList<String>();
			for(Branch branch : branches){
				branchInfo.add(branch.getName() +" "+branch.getRetailerName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.branch_search_results_list_row, branchInfo);
		setListAdapter(arrayAdapter);
	}
	
	/* An item in the list is selected */
	protected void onListItemClick(ListView listView, View view, int position, long id) {
    	super.onListItemClick(listView, view, position, id);
    	
    	Intent intent = new Intent(Intent.ACTION_VIEW);
		Bundle bundle = new Bundle();
		Log.d("branches.get(position)", branches.get(position).toString());
		bundle.putParcelable("branch", branches.get(position));
		intent.putExtras(bundle);
		intent.setClassName(getApplicationContext(), ViewBranchActivity.class.getName());
		startActivity(intent);
    }
}
